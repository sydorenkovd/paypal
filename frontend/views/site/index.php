<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use  yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <?php if (!Yii::$app->user->isGuest) { ?>
                <div class="btn btn-warning donate">
                    <a href="" style="text-decoration: none"><span>DONATE with PayPal</span></a>
                    <div class="row">
                        <div class="col-lg-9">
                            <?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => Url::to(['/site/donate'])]); ?>

                            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
                <?php } else { ?>
                    <div class="col-sm-8">
                        <h1 class="te">You have to be authenticated!</h1>
                        <div class="row">
                            <div class="col-sm-6"> <a class="btn btn-primary" href="<?= Url::to(['/site/signup'])?>">Sign UP!</a>
                                <a class="pull-right btn btn-primary" href="<?= Url::to(['/site/login'])?>">Sign IN!</a></div>
                        </div>

                    </div>
                <?php } ?>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</div>
