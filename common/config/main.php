<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => false,
            'showScriptName' => FALSE,
        ],
        'payPalClassic'    => [
            'class'        => 'kun391\paypal\ClassicAPI',
            'pathFileConfig' => '',
        ],
    ],
];
